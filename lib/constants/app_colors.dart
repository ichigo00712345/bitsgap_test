import 'package:flutter/material.dart';

abstract class AppColors {
  static const Color lightBackground = Color(0xfff9f9f9);
  static const Color lightForeground = Color(0xff262931);
  static const Color lightPrimary = Color(0xff4a55a7);
  static const Color lightSecondary = Color(0xffBFDBFE);
  static const Color darkBackground = Color(0xff181818);
  static const Color darkForeground = Color(0xffffffff);
  static const Color darkPrimary = Color(0xff6371DE);
  static const Color darkSecondary = Color(0xff1E293B);
  static const Color error = Color(0xffE12E0D);
  static const Color success = Color(0xff00B37E);
  static const Color btnActiveColor = Color(0xff1A5CFF);
}
