import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import '../../stores/form_store.dart';
import 'custom_form_field.dart';

class LoginForm extends StatelessWidget {
  final FormStore store;
  const LoginForm({super.key, required this.store});

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (_) {
        // store.successedSubmit ? showDialog(context: context, builder: (context) => const Text('123')) : null;
        return Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              CustomFormField(
                hintText: 'Username',
                onChanged: (value) => store.name = value,
                errorText: store.error.username,
              ),
              const SizedBox(height: 12),
              CustomFormField(
                hintText: 'Password',
                onChanged: (value) => store.password = value,
                errorText: store.error.password,
              ),
            ],
          ),
        );
      },
    );
  }
}

class SingUpForm extends StatelessWidget {
  final FormStore store;
  const SingUpForm({super.key, required this.store});

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (_) {
        return Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              CustomFormField(
                hintText: 'Email',
                onChanged: (value) => store.email = value,
                errorText: store.error.email,
              ),
              const SizedBox(height: 12),
              CustomFormField(
                hintText: 'Username',
                onChanged: (value) => store.name = value,
                errorText: store.error.username,
              ),
              const SizedBox(height: 12),
              CustomFormField(
                hintText: 'Password',
                onChanged: (value) => store.password = value,
                errorText: store.error.password,
              ),
            ],
          ),
        );
      },
    );
  }
}
