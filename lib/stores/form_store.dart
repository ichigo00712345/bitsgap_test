import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:mobx/mobx.dart';

part 'form_store.g.dart';

class FormStore = _FormStore with _$FormStore;

abstract class _FormStore with Store {
  final box = Hive.box('appBox');
  final FormErrorState error = FormErrorState();

  @observable
  bool isLogged = false;
  @observable
  String email = '';

  @observable
  String name = '';

  @observable
  String password = '';

  @computed
  bool get canSubmit => !error.hasErrors;

  @action
  submitLoginForm(BuildContext context) {
    if (canSubmit && box.containsKey('user')) {
      final Map<String, dynamic> userData = jsonDecode(box.get('user'));
      if (userData['password'] == password) {
        isLogged = true;
        box.put('currentLoggedStatus', true);
        print('submitLoginForm success');
      } else {
        showDialog(
          context: context,
          builder: (context) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [Center(child: Text('Ошибка логина'))],
            );
          },
        );
      }
    } else {
      print('submitLoginForm error');
      showDialog(
        context: context,
        builder: (context) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [Center(child: Text('Ошибка логина'))],
          );
        },
      );
    }
  }

  @action
  submitSignUpForm(BuildContext context) {
    if (canSubmit) {
      final Map<String, dynamic> userData = {'name': name, 'password': password};
      box.put('user', jsonEncode(userData));
      print('submitSignUpForm success');
      showDialog(
        context: context,
        builder: (context) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Center(
                child: Text('Вы успешно зарегистрированы'),
              ),
            ],
          );
        },
      );
    } else {
      showDialog(
        context: context,
        builder: (context) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [Center(child: Text('Ошибка регистрации'))],
          );
        },
      );
      print('submitSignUpForm error');
    }
  }

  late List<ReactionDisposer> _disposers;

  void setupValidations() {
    _disposers = [
      reaction((_) => name, validateUsername),
      reaction((_) => email, validateEmail),
      reaction((_) => password, validatePassword),
      autorun((_) {
        isLogged = box.get('currentLoggedStatus') ?? false;
      }),
    ];
  }

  @action
  Future validateUsername(String value) async {
    error.username = isNull(value) || value.isEmpty ? 'Cannot be blank' : null;
  }

  @action
  void validatePassword(String value) {
    error.password = isNull(value) || value.isEmpty ? 'Cannot be blank' : null;
  }

  @action
  void validateEmail(String value) {
    error.email = isEmail(value) ? null : 'Not a valid email';
  }

  void dispose() {
    for (final d in _disposers) {
      d();
    }
  }

  void validateAll() {
    validatePassword(password);
    validateEmail(email);
    validateUsername(name);
  }
}

class FormErrorState = _FormErrorState with _$FormErrorState;

abstract class _FormErrorState with Store {
  @observable
  String? email;

  @observable
  String? password;

  @observable
  String? username;

  @computed
  bool get hasErrors =>
      username == null || email == null || password == null || username == '' || email == '' || password != '';
}

bool isNull(dynamic value) => value == null;
bool isEmail(dynamic value) =>
    RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value);
