import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

part 'switch_button_store.g.dart';

enum CurrentSelectedButton { login, signUp }

class SwitchButtonStore = _SwitchButtonStore with _$SwitchButtonStore;

abstract class _SwitchButtonStore with Store {
  @observable
  CurrentSelectedButton currentSelectedButton = CurrentSelectedButton.login;

  @observable
  PageController pageController = PageController(initialPage: 0);
  @computed
  get isLoginActive => currentSelectedButton == CurrentSelectedButton.login;

  @computed
  int get currentPageInPageView {
    return currentSelectedButton == CurrentSelectedButton.login ? 0 : 1;
  }

  @action
  void toggleActiveButton() {
    currentSelectedButton == CurrentSelectedButton.login
        ? currentSelectedButton = CurrentSelectedButton.signUp
        : currentSelectedButton = CurrentSelectedButton.login;
    pageController.animateToPage(currentPageInPageView,
        duration: const Duration(milliseconds: 300), curve: Curves.linear);
  }
}
