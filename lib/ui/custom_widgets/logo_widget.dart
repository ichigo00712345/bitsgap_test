import 'package:flutter/material.dart';

class LogoWidget extends StatelessWidget {
  const LogoWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 49,
      height: 48,
      child: Image.asset('assets/images/logo.png'),
    );
  }
}
