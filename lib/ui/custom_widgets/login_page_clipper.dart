import 'package:flutter/material.dart';

class LoginPageClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var conicControlPoint = Offset(size.width - 80, size.height * 0.31 - 100);
    var conicEndPoint = Offset(0, size.height * 0.31);
    Path path = Path()
      ..lineTo(size.width, 0)
      ..lineTo(size.width, size.height * 0.31)
      ..conicTo(conicControlPoint.dx, conicControlPoint.dy, conicEndPoint.dx, conicEndPoint.dy, 3);

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
