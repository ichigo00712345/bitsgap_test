import 'package:flutter/material.dart';

class CustomFormField extends StatelessWidget {
  final String hintText;
  final String? errorText;
  final ValueChanged onChanged;
  const CustomFormField({
    super.key,
    required this.hintText,
    required this.onChanged,
    this.errorText,
  });

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return TextField(
      onChanged: onChanged,
      style: theme.textTheme.bodyMedium,
      decoration: InputDecoration(
        errorText: errorText,
        hintText: hintText,
        hintStyle: theme.textTheme.bodyMedium,
        enabledBorder: theme.inputDecorationTheme.border?.copyWith(
          borderSide: BorderSide(
            color: theme.colorScheme.primary,
          ),
        ),
        contentPadding: const EdgeInsets.symmetric(horizontal: 16),
      ),
    );
  }
}
