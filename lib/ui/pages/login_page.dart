import 'package:bitsgap_test/stores/form_store.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import '../../stores/switch_button_store.dart';
import '../custom_widgets/forgot_password_widget.dart';
import '../custom_widgets/login_page_clipper.dart';
import '../custom_widgets/login_page_forms.dart';
import '../custom_widgets/logo_widget.dart';
import '../custom_widgets/switch_buttons_row.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final switchButtonStore = SwitchButtonStore();
  final loginStore = FormStore();
  final signUpStore = FormStore();
  @override
  void initState() {
    super.initState();
    loginStore.setupValidations();
    signUpStore.setupValidations();
  }

  @override
  void dispose() {
    loginStore.dispose();
    signUpStore.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.background,
      body: SingleChildScrollView(
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: [
              ClipPath(
                clipper: LoginPageClipper(),
                child: Container(
                  color: Theme.of(context).colorScheme.secondary,
                ),
              ),
              SafeArea(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  child: Observer(
                    builder: (_) {
                      return loginStore.isLogged
                          ? const Center(
                              child: Text('You are logged in'),
                            )
                          : Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                const SizedBox(height: 55),
                                const Align(
                                  alignment: Alignment.topLeft,
                                  child: LogoWidget(),
                                ),
                                const Spacer(),
                                SizedBox(
                                  width: MediaQuery.of(context).size.width,
                                  height: 240,
                                  child: PageView(
                                    controller: switchButtonStore.pageController,
                                    physics: const NeverScrollableScrollPhysics(),
                                    children: [
                                      LoginForm(
                                        store: loginStore,
                                      ),
                                      SingUpForm(
                                        store: signUpStore,
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(height: 30),
                                SwitchButtonsRow(
                                  signUpStore: signUpStore,
                                  loginStore: loginStore,
                                  switchButtonStore: switchButtonStore,
                                ),
                                const SizedBox(height: 42),
                                const ForgotPasswordWidget(),
                                const SizedBox(height: 180),
                              ],
                            );
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
