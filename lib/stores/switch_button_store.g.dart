// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'switch_button_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$SwitchButtonStore on _SwitchButtonStore, Store {
  Computed<dynamic>? _$isLoginActiveComputed;

  @override
  dynamic get isLoginActive =>
      (_$isLoginActiveComputed ??= Computed<dynamic>(() => super.isLoginActive,
              name: '_SwitchButtonStore.isLoginActive'))
          .value;
  Computed<int>? _$currentPageInPageViewComputed;

  @override
  int get currentPageInPageView => (_$currentPageInPageViewComputed ??=
          Computed<int>(() => super.currentPageInPageView,
              name: '_SwitchButtonStore.currentPageInPageView'))
      .value;

  late final _$currentSelectedButtonAtom =
      Atom(name: '_SwitchButtonStore.currentSelectedButton', context: context);

  @override
  CurrentSelectedButton get currentSelectedButton {
    _$currentSelectedButtonAtom.reportRead();
    return super.currentSelectedButton;
  }

  @override
  set currentSelectedButton(CurrentSelectedButton value) {
    _$currentSelectedButtonAtom.reportWrite(value, super.currentSelectedButton,
        () {
      super.currentSelectedButton = value;
    });
  }

  late final _$pageControllerAtom =
      Atom(name: '_SwitchButtonStore.pageController', context: context);

  @override
  PageController get pageController {
    _$pageControllerAtom.reportRead();
    return super.pageController;
  }

  @override
  set pageController(PageController value) {
    _$pageControllerAtom.reportWrite(value, super.pageController, () {
      super.pageController = value;
    });
  }

  late final _$_SwitchButtonStoreActionController =
      ActionController(name: '_SwitchButtonStore', context: context);

  @override
  void toggleActiveButton() {
    final _$actionInfo = _$_SwitchButtonStoreActionController.startAction(
        name: '_SwitchButtonStore.toggleActiveButton');
    try {
      return super.toggleActiveButton();
    } finally {
      _$_SwitchButtonStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
currentSelectedButton: ${currentSelectedButton},
pageController: ${pageController},
isLoginActive: ${isLoginActive},
currentPageInPageView: ${currentPageInPageView}
    ''';
  }
}
