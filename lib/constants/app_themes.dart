import 'package:flutter/material.dart';

import 'app_colors.dart';

abstract class AppThemes {
  static ThemeData darkTheme = ThemeData(
    textButtonTheme: _textButtonThemeData(),
    fontFamily: _fontFamily,
    colorScheme: const ColorScheme(
      brightness: Brightness.dark,
      primary: AppColors.darkPrimary,
      onPrimary: Colors.white,
      secondary: AppColors.darkSecondary,
      onSecondary: Colors.white,
      error: AppColors.error,
      onError: Colors.white,
      background: AppColors.darkBackground,
      onBackground: Colors.white,
      surface: AppColors.darkForeground,
      onSurface: Colors.black,
    ),
    textTheme: _getTextTheme(),
    inputDecorationTheme: _inputDecorationTheme(),
  );

  static ThemeData lightTheme = ThemeData(
    colorScheme: const ColorScheme(
      brightness: Brightness.light,
      primary: AppColors.lightPrimary,
      onPrimary: Colors.white,
      secondary: AppColors.lightSecondary,
      onSecondary: Colors.black,
      error: AppColors.error,
      onError: Colors.white,
      background: AppColors.lightBackground,
      onBackground: Colors.black,
      surface: AppColors.lightForeground,
      onSurface: Colors.black,
    ),
    textButtonTheme: _textButtonThemeData(),
    fontFamily: _fontFamily,
    textTheme: _getTextTheme(),
    inputDecorationTheme: _inputDecorationTheme(),
  );

  static const String _fontFamily = 'TT Norms Pro';

  static TextTheme _getTextTheme() {
    return const TextTheme(
      labelSmall: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: 14,
      ),
      bodyMedium: TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: 16,
      ),
    );
  }

  static TextButtonThemeData _textButtonThemeData() {
    return TextButtonThemeData(
      style: ButtonStyle(
        overlayColor: MaterialStateProperty.all(
          Colors.transparent,
        ),
      ),
    );
  }

  static InputDecorationTheme _inputDecorationTheme() {
    return InputDecorationTheme(
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(24),
      ),
    );
  }
}
