import 'package:bitsgap_test/stores/form_store.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:provider/provider.dart';

import 'constants/app_themes.dart';
import 'stores/switch_button_store.dart';
import 'ui/pages/login_page.dart';

void main() async {
  await Hive.initFlutter();
  await Hive.openBox('appBox');
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Bitsgap test app',
      theme: AppThemes.lightTheme,
      darkTheme: AppThemes.darkTheme,
      home: MultiProvider(
        providers: [
          Provider<FormStore>(
            create: (_) => FormStore(),
          ),
          Provider<SwitchButtonStore>(
            create: (_) => SwitchButtonStore(),
          ),
          Provider<Box>(create: (_) => Hive.box('appBox')),
        ],
        child: const LoginPage(),
      ),
    );
  }
}
