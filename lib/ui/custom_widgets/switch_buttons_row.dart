import 'package:bitsgap_test/stores/form_store.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import '../../constants/app_colors.dart';
import '../../stores/switch_button_store.dart';

class SwitchButtonsRow extends StatelessWidget {
  final FormStore loginStore;
  final FormStore signUpStore;
  final SwitchButtonStore switchButtonStore;
  const SwitchButtonsRow(
      {super.key, required this.switchButtonStore, required this.loginStore, required this.signUpStore});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Theme.of(context).colorScheme.secondary,
      ),
      child: Observer(
        builder: (context) => Row(
          children: [
            Expanded(
              child: SwitchableButton(
                toggleButton: switchButtonStore.toggleActiveButton,
                submitForm: () {
                  loginStore.submitLoginForm(context);
                },
                buttonText: 'Login',
                isActive: switchButtonStore.isLoginActive,
              ),
            ),
            Expanded(
              child: SwitchableButton(
                toggleButton: switchButtonStore.toggleActiveButton,
                submitForm: () {
                  signUpStore.submitSignUpForm(context);
                },
                buttonText: 'Sign-up',
                isActive: !switchButtonStore.isLoginActive,
              ),
            )
          ],
        ),
      ),
    );
  }
}

class SwitchableButton extends StatelessWidget {
  final VoidCallback toggleButton;
  final VoidCallback submitForm;
  final bool isActive;
  final String buttonText;
  const SwitchableButton({
    super.key,
    required this.buttonText,
    required this.isActive,
    required this.toggleButton,
    required this.submitForm,
  });

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return GestureDetector(
      onTap: isActive ? submitForm : toggleButton,
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 300),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: isActive ? AppColors.btnActiveColor : Colors.transparent,
        ),
        child: Center(
          child: Text(
            buttonText,
            style: theme.textTheme.labelSmall!.copyWith(
              color: isActive ? Colors.white : theme.textTheme.labelSmall?.color,
            ),
          ),
        ),
      ),
    );
  }
}
